/**
 * Configuración del puerto
 * 
 * si el process.env.PORT no esta configurado se le asigna el puerto 3000
 */

process.env.PORT = process.env.PORT || 3000;
