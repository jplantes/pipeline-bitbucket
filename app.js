/* Configuraciones globales */
require('./config');

/* Configuraciones Express */
const express = require('express');
const app = express();

/* Configuraciones body-parser */
const bodyParser = require('body-parser');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.get('/', (req, res) => {

  res.status(200).json({
    ok: true,
    mensaje: 'Ya esta desplegado correctamente'
  });

});


app.listen(process.env.PORT, () => {
  console.log(`Corriendo en el puerto ${ process.env.PORT }`);
});